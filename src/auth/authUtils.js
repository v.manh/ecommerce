'use strict';
const JWT = require('jsonwebtoken')
const createTokenPair = async (payload, publicKey, privateKey) =>{
    try {
        // create accessToken + refeshToken
        console.log('accessToken')
        const accessToken = JWT.sign(payload, privateKey, {
            expiresIn: '24h'
        })
        console.log('refeshToken')
        const refeshToken = JWT.sign(payload, privateKey, {
            expiresIn: '7 days'
        })
        console.log('veriry')
        //veriry 
        JWT.verify(accessToken, publicKey, (err, decode) => {
            if(err){
                console.log(`error verifying access token`,err)
            }else {
                console.log(`decode verify`, decode)
            }
        })
        return {accessToken, refeshToken}
    } catch (error) {
        
    }
}

module.exports = {
    createTokenPair
}