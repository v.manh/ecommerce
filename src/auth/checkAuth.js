'use strict';

const { findById } = require("../services/apikey.service");


const HEADER = {
    API_KEY: 'x-api-key',
    AUTHORIZATION: 'authorization'
}

const apiKey = async (req, res, next) => {
    try {
        const key = req.headers[HEADER.API_KEY]?.toString()
        if(!key){
            return res.status(403).json({
                status: 403,
                message: 'Forbidden Error'
            })
        }
        // check objectKey
        const objectKey = await findById(key)
        if(!objectKey){
            return res.status(403).json({
                status: 403,
                message: 'Forbidden Error'
            })
        }
        req.objectKey = objectKey
        return next()

    } catch (error) {
        
    }
}

const permission = (permission) => { 
    console.log(`check permission:: ${permission}`)
    return (req, res, next) => {
        if(req.objectKey.permissions.includes(permission)){
            return next()
        }
        return res.status(403).json({
            status: 403,
            message: 'permission denied'
        })
    }
}

module.exports = { apiKey, permission }