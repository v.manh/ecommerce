'use strict'

const { json } = require("express")
const { createTokenPair } = require("../auth/authUtils")
const AccessService = require("../services/access.service")

class AccessController {
    signUp = async (req, res) =>{
        try {
            console.log(`[P]::signUp::`, req.body)
            return res.status(200).json(await AccessService.signUp(req.body)) 
        } catch (error) {
            next(error)
        }
    }
}

module.exports = new AccessController()