const app = require("./src/app")
const port = process.env.PORT || 3001;

console.log("process.env.PORT:",process.env.PORT)

const server =  app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
})

process.on('SIGINT', ()=>{
    server.close (()=>console.log('exit server express'))
})